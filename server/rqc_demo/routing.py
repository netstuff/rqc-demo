from django.urls import path, re_path
from channels.http import AsgiHandler
from channels.routing import ProtocolTypeRouter, URLRouter

from rqc_demo.consumers import SolveConsumer


application = ProtocolTypeRouter({
    'websocket': URLRouter([
        re_path(r'solver/$', SolveConsumer),
    ]),
})
