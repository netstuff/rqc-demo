import React from 'react';
import './App.css';
import Demo from './Demo';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Demo></Demo>
      </header>
    </div>
  );
}

export default App;
