import React from 'react';
import moment from 'moment';
import { LineChart, CartesianGrid, XAxis, YAxis, Line, Label, LabelList } from 'recharts';

const SOLVER_URI = 'ws://188.227.75.61:8000/solver/';

export default class Demo extends React.Component {
  constructor (props) {
    super(props);

    this.socket = undefined;
    this.state = {
      chartData: [],
      started: false,
    }
  }

  initSocket (uri) {
    this.socket = new WebSocket(uri);

    this.socket.addEventListener('open', (event) => {
      console.log(`${uri} has opened`);
      this.socket.send('Buongiorno!');
    });

    this.socket.addEventListener('close', (event) => {
      console.log(`${uri} has closed`);
    });

    this.socket.addEventListener('error', (error) => {
      console.error(error);
    });

    this.socket.addEventListener('message', (event) => {
      const { started } = JSON.parse(event.data);
      let { timestamp, energy } = JSON.parse(event.data);

      if (started) {
        this.setState({ started: true });
        console.log(`Solver started`);
      }
      else if (timestamp && energy) {
        energy = Math.round(energy * 100) / 100;

        this.setState({
          chartData: [...this.state.chartData, {
            timestamp,
            energy,
          }],
        });
        console.log(`Solve updated`);
      }
    });
  }

  initSolver = () => {
    if (!this.socket || this.socket.readyState > 1) {
      this.initSocket(SOLVER_URI);
    }

    if (this.socket.readyState === 0) {
      setTimeout(() => {
        this.initSolver();
      }, 100);
    } else {
      this.socket.send('Go!');
    }
  }

  minEnergy = () => {
    return (this.state.chartData.length)
      ? Math.min(...this.state.chartData.map((el) => el.energy))
      : undefined;
  }

  formatXAxis = (tick) => {
    return moment.unix(tick).format('MMM DD HH:mm');
  }

  render () {
    return (
      <div className="demo">
        <div className="field">
          <button onClick={ this.initSolver }>Start</button>
        </div>


        <LineChart width={600} height={400} data={this.state.chartData} syncId="test">
            <CartesianGrid stroke="#f5f5f5" fill="#e6e6e6" />
            <YAxis type="number" dataKey="energy" height={40}>
              <Label value="y" position="insideLeft" />
            </YAxis>
            <XAxis type="category" dataKey="timestamp" tickFormatter={this.formatXAxis} width={80} angle={90}>
              <Label value="x" position="insideBottom" />
            </XAxis>
            <Line
              key="energy"
              type="monotone"
              dataKey="energy"
              stroke="#ff7300"
              strokeOpacity={100}
              strokeDasharray="3 3"
            >
              <LabelList position="bottom" offset={10} dataKey="name" />
            </Line>
          </LineChart>

          <div>
            Min energy: {this.minEnergy()}
          </div>
        </div>
    );
  }
}