import qboard
import numpy as np

from datetime import datetime
from django.http import JsonResponse

from channels.generic.websocket import JsonWebsocketConsumer


class SolveConsumer(JsonWebsocketConsumer):
    """A demo solving websocket consumer."""
    def init(self):
        """Initiate random matrix and solver for it."""
        self.matrix = np.random.rand(30, 30) - 0.5
        self.solver = qboard.solver(mode='bf')
        self.spins = None
        self.energy = None

    def clear(self):
        """Reset generated matrix and solver."""
        self.energy = None
        self.matrix = None
        self.solver = None
        self.spins = None
    
    def connect(self):
        """Webscoket connection handler."""
        self.init()
        self.accept()
    
    def receive(self, text_data=None):
        """Receive messages from client."""
        if text_data == 'Buongiorno!':
            print('Client connected')

        elif text_data == 'Go!':
            self.send_json({
                'started': datetime.utcnow().isoformat()
            })
            self.start()
    
    def disconnect(self, close_code):
        """Webscoket disconnection handler."""
        self.clear()

    def send_solve(self):
        """Send solve to client."""
        self.send_json({
            'timestamp': datetime.now().strftime("%s"),
            'energy': self.energy.tolist(),
        })
    
    def start(self):
        """Starts solver."""
        self.spins, self.energy = self.solver.solve_qubo(self.matrix, callback=self.callback, timeout=5, verbosity=0)
        self.send_solve()

    def callback(self, dic):
        """Solver callback function."""
        cb_type = dic.get('cb_type')

        if cb_type == qboard.constants.CB_TYPE_NEW_SOLUTION:
            print('New solution found, energy {energy}, result vector {spins}'.format(**dic))

        if cb_type == qboard.constants.CB_TYPE_INTERRUPT_TIMEOUT:
            print('Solver interrupted by timeout')
            self.close()

        if cb_type == qboard.constants.CB_TYPE_INTERRUPT_TARGET:
            print('Solver interrupted by target')
            self.close()

        self.spins = dic.get('spins')
        self.energy = dic.get('energy')
        self.send_solve()
